
/*
 * Class for calculating distance based amplitude panning
 * Code based on: http://www.pnek.org/wp-content/uploads/2010/04/icmc2009-dbap.pdf
 *
 * Author: Jakob Schloetter <jakob@jrkb.land>
 *
 * TODO:
 * - introduce speaker weights
 * - optimize calculations
 * - convert class as : UGen?
 */
DBAPCalculator {
  *secret {
    ^("This class is neither finished, tested or otherwise in a good condition.");
  }

  // r is spatial blur coefficient
  *cDistance { arg a, b, r=0.001;
    var xd = a[0]-b[0];
    var yd = a[1]-b[1];
    var zd = a[2]-b[2];
    ^((xd*xd) + (yd*yd) + (zd*zd) + (r*r)).sqrt;
  }

  // r is spatial blur coefficient
  *cDistance2D { arg a, b, r=0.001;
    var xd = a[0]-b[0];
    var yd = a[1]-b[1];
    ^((xd*xd) + (yd*yd) + (r*r)).sqrt;
  }


  *cDistances { arg src, speakers;
    var distances = Array.new;
    speakers.do({ arg speaker, i;
      var distance;
      if (src.size == 2,
          { distance = this.cDistance2D(src,speaker); },
          { distance = this.cDistance(src,speaker); }
      );
      distances = distances.add(distance);
    });
    ^distances;
  }

  // r = rolloff coefficient. 6 for free field, 3-5 for semi-closed/closed
  *cA { arg rolloff=4;
    ^(10.0*(-1.0*rolloff/20.0));
  }

//coefficient dependent on all speakers to ensure sum of amplitudes is 1
  *cK { arg distances, a;
    var ks = Array.new;
    distances.do({arg item, i;
      ks = ks.add(1/(item*item));
    });
    ^((2.0*a)/(ks.sum).sqrt)
  }

  *cAmplitude { arg k,d,a;
    ^(k/(2*d*a));
  }

  // this is the function you are supposed to call
  *cAmplitudes { arg src=[0,0,0], speakers=[[-1,0,0],[1,0,0]];
    var distances = this.cDistances(src,speakers);
    var amplitudes = Array.new;
    var a = this.cA;
    var k;
    k = this.cK(distances,a);
    speakers.do({ arg speaker, i;
      var amplitude = this.cAmplitude(k,distances[i],a);
      amplitudes = amplitudes.add(amplitude);
    });
    ^amplitudes;
  }
}
