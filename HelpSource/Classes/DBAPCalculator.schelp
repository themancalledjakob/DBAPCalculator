class:: DBAPCalculator
summary:: Distance Based Amplitude Panning
categories:: Ugens>Multichannel>Panners

description::
based on: http://www.pnek.org/wp-content/uploads/2010/04/icmc2009-dbap.pdf

classmethods::

method:: cA
calculate a coefficient
argument:: rolloff
rolloff coefficient, default is set to 4.

method:: cK
coefficient dependent on all speakers to ensure sum of amplitude is 1
argument:: distances
array of distances of source to each speaker
argument:: a
a coefficient, see cA

method:: cAmplitude
calculate amplitude of individual speaker
argument:: k
coefficient dependent on all speakers to ensure sum of amplitude is 1
argument:: d
distance source to speaker
argument:: a
coefficient

method:: cAmplitudes
calculate array of amplitudes for all speakers. This is the function you are supposed to call.
argument:: src
position of source
[float,float]
argument:: speakers
array of distances between source and each speaker
[[float,float], ..., [float,float]]

method:: cDistance
calculate distance between two positions. Uses square root, could be a bottleneck.
argument:: a
position one
argument:: b
position two
argument:: r
spatial blur coefficient. Preventing that a source is played solely on one speaker. Default is 0.001.

method:: cDistances
calculate array of distances from source to each speaker
argument:: src
position of source
argument:: speakers
array of speaker positions, directly passed from cAmplitudes

method:: secret
undocumented


examples::
code::
{
var speakers, source, amplitudes;
source = [0,0]; // x,y coordinates of source position
speakers = [[1,0],[0.4,0.2],[12,9]]; // x,y coordinates of speaker positions
amplitudes = DBAPCalculator.cAmplitudes(source,speakers);
}.value;

// should give you the amplitudes of each speaker as [ 0.40809785084964, 0.9125327109096, 0.027206536932776 ]

// Or... try 3d?
{
var speakers, source, amplitudes;
source = [0,0,0]; // x,y coordinates of source position
speakers = [[1,0,1],[0.4,0.2,3],[12,9,-1]]; // x,y coordinates of speaker positions
amplitudes = DBAPCalculator.cAmplitudes(source,speakers);
}.value;

// should give you the amplitudes of each speaker as [ 0.61817595901325, 0.60084046700933, 0.50680293695091 ]

::
