# DBAPCalculator
Supercollider class for Distance Based Amplitude Panning
based on: http://www.pnek.org/wp-content/uploads/2010/04/icmc2009-dbap.pdf

`Distance-based amplitude panning (DBAP) offers an alternative panningbased
spatialization method where no assumptions are made concerning the layout of the speaker array nor the position of the listener`

## installation
- put in your Extension folder

    in Linux it's ~/.local/share/SuperCollider/Extensions

   Google knows your operating system

- recompile class library or restart SuperCollider

## usage
```
{
var speakers, source, amplitudes;
source = [0,0]; // these are x,y coordinates of the source position
speakers = [[1,0],[0.4,0.2],[12,9]];
amplitudes = DBAPCalculator.cAmplitudes(source,speakers);
}.value;
```
this should give you
```
[ 0.40809785084964, 0.9125327109096, 0.027206536932776 ]
```

## test
```
(
{
	var test = DBAPCalculator.cAmplitudes(source:[0,0],speakers:[[4,0],[0,4],[10,0]]);
	var out = Array.new;
	test.do({arg item, i;
		out = out.add(item*item);
	});
	out.sum; // should be 1!! btw, this indicates that the overall amplitude of a source is always 1
}.value;
)
```


# to do
- 3D
- speaker weights
- make a UGen, why not
- add helpfile
- implement distance based amplitude (independent from panning) to be able to make things move closer
